Requirements define constraints on template parameters. There are four types
of requirements:
    itemization(
    it() Constraints on em(facilities) (e.g., requiring the availability of
        tt(operator<));
    it() Constraints on em(types) (e.g., requiring the existence of
        tt(Type::value_type) where tt(Type) is a concept template type
        parameter);
    it() Constraints on relations between variables;
    it() Constraints on the relationships between differen concept template
        parameters. 
    )

In all cases these constraints must be compile-time verifiable.

When multiple constraints are specified, they must em(all) be satisfied before
the template argument types are accepted by the compiler. Multiple constraints
can also be combined using the boolean operators tt(and, or) and tt(not). In a
generic concept function definition the constraints are listed following the
tt(return) keyword:
        verb(
    template <typename Type> 
    concept bool Constraints()
    {
        return ...constraint(s) are defined here... ;
    }
        )

The constraints are combinations of hi(requires expression)tt(requires)
expressions. Requires expressions look like function calls, defining
parameters of the types specified in the concept's template header. E.g.,
        verb(
    template <typename Type> 
    concept bool Constraints()
    {
        return requires(Type param)
                {
                    ...constraint(s) are specified here...
                };
    }
        )
    The parameters of requires-expressions are purely formal: they do not have
default arguments, and parameter lists of requires-expressions may not end in
ellipses. If a requirement specification results in an invalid type or an
invalid semantic meaning then the requirement evaluates to tt(false).



